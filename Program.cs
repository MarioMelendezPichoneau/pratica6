﻿using System;

namespace pratica6
{

    //Crear una clase Persona que tenga como atributos el "cedula, nombre, apellido y la edad 
    //  Definir como responsabilidad un método para mostrar ó imprimir.
    // Crear una segunda clase Profesor que herede de la clase Persona. Añadir un atributo sueldo 
    //( y su propiedad) y el método para imprimir su sueldo. Definir un objeto de la clase Persona y 
    //llamar a sus métodos y propiedades. También crear un objeto de la clase Profesor y llamar a sus métodos y 
    // propiedades.

    class Program
    {
        static void Main(string[] args)
        {
            Persona nadie = new Persona();
            nadie.Nombre = "Mario";
            nadie.Apellido = "Melendez pichoneau";
            nadie.Edad = 18;
            nadie.Cedula = "402-4587518-8";
            Console.WriteLine(" los datos de la persona son: ");
            nadie.Imprimir();

            Profesor antes = new Profesor();
            antes.Nombre = "Miguel";
            antes.Apellido = "Moreta";
            antes.Edad = 20;
            antes.Cedula = "010-4587518-8";
            antes.Sueldo = 40000;
            Console.WriteLine(" \n Losdatos del maestro son: ");
            antes.Imprimir();
            antes.Mostra();


            
            
        }

       
    }

    public class Persona
    {

        private string nombre;
        private string apellido;
        private int edad;
        private string cedula;

        
        public string Nombre
        {
            get
            {
                return nombre;

            }
            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;

            }
            set
            {
                apellido = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;

            }
            set
            {
                edad = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula ;

            }
            set
            {
                cedula = value;
            }
        }

        public void Imprimir()
        {
            Console.WriteLine($" Nombre: {nombre} ");
            Console.WriteLine($" Apellido: {apellido} ");
            Console.WriteLine($" Edad: {edad} ");
            Console.WriteLine($" cedula: {cedula} ");
        }



        

    }

    public  class Profesor : Persona 
    {
        private float sueldo;
        

        public float Sueldo
        {
            get
            {
                return sueldo;

            }
            set
            {
                sueldo = value;
            }
        }

        public void Mostra()
        {
            Console.WriteLine($" su sueldo es: {sueldo} ");
        }
    }
}
